Настраиваем Server и Client BAREOS

для настройки подключения на стороне сервера необходимо:
1.  Файл /etc/bareos/bareos-fd.d/director/bareos-dir.conf
2.  Добавить клиент можно через bconsole, с помощью команды

configure add client name=vmi123456.contaboserver.net address=5.123.123.248 password=hKVxEc7oier57lotfyRnQ9K

или

configure add client name=vps654321.ovh.net address=123.123.123.248 password=hKVxEc7oier57lotfyRnQ9K

3.  В папке /etc/bareos/bareos-dir-export/client/ появиться клиент  vps654321.ovh.net
4.  Заходим в /etc/bareos/bareos-dir-export/client/vps654321.ovh.net/bareos-fd.d/director/bareos-dir.conf

    и копируем содержимое с сервера на клиент
    
    в дирректорию root@vps654321:/etc/bareos/bareos-fd.d/director/bareos-dir.conf
    
5.  В дирректории /etc/bareos/bareos-dir.d/pool/ настраиваем полный, инкрементный и дифиринциальный backup.

        vps654321Full.conf
        vps654321Incremental.conf
        vps654321Differential.conf
6.  В дирректории /etc/bareos/bareos-dir.d/jobdefs/ настраиваем jobdefs

        vps654321-job.conf
7.  В дирректории /etc/bareos/bareos-dir.d/job/  настраиваем job

        backup-vps654321.conf
8.  После добавления клиента сервер BAREOS необходимо перезапустить.

    service bareos-fd restart
    
    service bareos-sd restart
    
    service bareos-dir restart
    
